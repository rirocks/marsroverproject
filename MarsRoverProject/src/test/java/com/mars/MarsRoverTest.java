package com.mars;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

/**
 * Unit test for MarsRover.
 */
public class MarsRoverTest {

	private Rover rover;

	@Before
	public void beforeRoverTest() {
		rover = new Rover(1,1,'N');
		rover.setDimensions(new String[]{"2", "2"});
	}

	@Test
	public void newRoverInstanceWithCoordinatesAndHeadingTest() {
		String expected = "1 1 N";
		assertEquals(expected, rover.printPosition());
	}	

	@Test
	public void roverShouldMoveOnHeading() {
		String expected = "1 2 N";
		rover.move();
		assertEquals(expected, rover.printPosition());
	}

	@Test
	public void roverShouldChangeHeading() {
		String expected = "1 1 E";
		String expected2 = "1 1 N";
		rover.rotate('R');
		assertEquals(expected, rover.printPosition());

		rover.rotate('L');
		assertEquals(expected2, rover.printPosition());

	}

	@Test
	public void roverShouldMoveForwardInHeadingDirectionAndStaysInPlateauTest() {
		String expected = "1 2 N";
		rover.move();
		assertEquals(expected, rover.printPosition());

		rover.move();
		assertEquals(expected, rover.printPosition());
	}

	@Test
	public void roverShouldntMoveOnUnknownCommandTest() {
		String expected = "1 1 N";
		rover.rotate('S');
		assertEquals(expected, rover.printPosition());
	}
	
}
