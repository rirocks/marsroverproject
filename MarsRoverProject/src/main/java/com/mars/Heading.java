package com.mars;

public enum Heading {

	NORTH(0, 'N'),
	EAST(1, 'E'),
	SOUTH(2, 'S'),
	WEST(3, 'W');

	private int value;
	private char direction;

	/**
	 * Constructor to define a Heading object.
	 * 
	 * @param val: numeric value of heading.
	 * @param dir: char value of heading.
	 */
	private Heading(int val, char dir) {
		value = val;
		direction = dir;
	}	

	/**
	 * Get numeric value of heading.
	 * 
	 * @return: Heading numeric value.
	 */
	public int getValue() {
		return this.value;
	}

	/**
	 * Get direction of heading.
	 * 
	 * @return: Heading direction char.
	 */
	public char getDirection() {
		return this.direction;
	}
}
