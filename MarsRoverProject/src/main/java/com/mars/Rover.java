package com.mars;

/**
 * This is the Rover class, and represents the object that moves
 * and shifts its position through the plateau.
 */
public class Rover {

	private int gridDimensionX;
	private int gridDimensionY;
	private int coordX;
	private int coordY;
	private Heading head;

	/**
	 * This is a constructor method to define a Rover object.
	 * 
	 * @param x: This represents the position on X axis on the plateau.
	 * @param y: This represents the position on Y axis on the plateau.
	 * @param h: This represents the cardinal point the object is facing.
	 */
	public Rover(int x, int y, char h) {
		this.coordX = x;
		this.coordY = y;
		switch(h) {
		case 'N': 
			this.head = Heading.NORTH;
			break;
		case 'E':
			this.head = Heading.EAST;
			break;
		case 'S':
			this.head = Heading.SOUTH;
			break;
		case 'W':
			this.head = Heading.WEST;
			break;
		}
	}

	/**
	 * Move the Rover on the plateau.
	 */
	public void move() {
		switch(this.head.getDirection()) {
		case 'N': 
			this.coordY = (this.coordY+1 <= this.gridDimensionY) ? this.coordY+1 : this.coordY;
			break;
		case 'E':
			this.coordX = (this.coordX+1 <= this.gridDimensionX) ? this.coordX+1 : this.coordX;
			break;
		case 'S':
			this.coordY = (this.coordY-1 >= 0) ? this.coordY-1 : this.coordY;
			break;
		case 'W':
			this.coordX = (this.coordX-1 >= 0) ? this.coordX-1 : this.coordX;
			break;
		}
	}
	
	/**
	 * Turn the Rover heading to the right
	 */
	public void turnRight() {
		int currentHeadValue = this.head.getValue();
		this.head = currentHeadValue < 3 ? Heading.values()[++currentHeadValue] : 
			Heading.values()[0];
	}

	/**
	 * Turn the Rover heading to the left
	 */
	public void turnLeft() {
		int currentHeadValue = this.head.getValue();
		this.head = currentHeadValue > 0 ? Heading.values()[--currentHeadValue] : 
			Heading.values()[3];
	}

	/**
	 * Rotate the Rover heading.
	 * 
	 * @param rotPos: Direction of the rotation L or R.
	 */
	public void rotate(char rotPos) {
		if (rotPos == 'R') {
			this.turnRight();
		} else if (rotPos == 'L') {
			this.turnLeft();
		}
	}


	/**
	 * Process all the commands the Rover most execute.
	 * 
	 * @param commandLine: Line with all the Rover commands.
	 */
	public void processCommands(String commandLine) {
		for (int i=0; i < commandLine.length(); i++){
			char singleCommand = commandLine.charAt(i);
			switch(singleCommand){
			case 'M':
				this.move();
				break;
			case 'L' :
			case 'R' :	
				this.rotate(singleCommand);
				break;
			}
		}
	}

	
	/**
	 * Sets Rover's plateau dimensions
	 * 
	 * @param dimensions: Max dimensions of X and Y axis of the plateau.
	 */
	public void setDimensions(String[] dimensions) {
		this.gridDimensionX = Integer.parseInt(dimensions[0]);
		this.gridDimensionY = Integer.parseInt(dimensions[1]);
	}

	/**
	 * Prints Rover position.
	 */
	public String printPosition() {
		String location = this.coordX+" "+this.coordY+" "+this.head.getDirection();
		return location;
	}
}
