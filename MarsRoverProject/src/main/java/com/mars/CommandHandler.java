package com.mars;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;

public class CommandHandler {

	private final ArrayList<Rover> roverList = new ArrayList<Rover>();
	private volatile boolean finishReading = false;

	public CommandHandler() {}

	public void startMission(final String filePath) {
		this.roverProducer(filePath);
		this.roverConsumer();
	}

	public void roverProducer(final String filePath) {

		Thread producer = new Thread() {
			public void run() {
				BufferedReader br = null;
				FileReader fr = null;

				try {
					fr = new FileReader(filePath);
					br = new BufferedReader(fr);

					String[] gridDimension;
					String roverLine;
					String commandLine;

					gridDimension = br.readLine().split(" ");

					roverLine = br.readLine();
					while (roverLine != null) {
						commandLine = br.readLine();
						if (validatePosition(roverLine, gridDimension))
							processRover(roverLine, commandLine, gridDimension);
						roverLine = br.readLine();
					}
					finishReading = true;

				} catch (IOException e) {
					e.printStackTrace();
				} finally {
					try {
						if (br != null)
							br.close();
						if (fr != null)
							fr.close();
					} catch (IOException ex) {
						ex.printStackTrace();
					}
				}
			}
		};

		producer.start();
	}

	public void roverConsumer() {

		Thread consumer = new Thread() {
			public void run() {
				FileWriter outputFile;
				try {
					outputFile = new FileWriter("output.txt");
					PrintWriter outputWriter = new PrintWriter(outputFile);

					while(!finishReading || roverList.size() > 0) {
						if (roverList.size() > 0) {
							Rover printRover = roverList.remove(0);
							outputWriter.println(printRover.printPosition());
						}
					}
					outputWriter.close();

				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		};
		consumer.start();
	}

	private void processRover(String roverLine, String commandLine, String[] gridDimensions) {

		Rover newRover = this.createRover(roverLine);
		newRover.setDimensions(gridDimensions);
		newRover.processCommands(commandLine);
		roverList.add(newRover); 
	}

	private Rover createRover(String roverLine) {
		String[] roverInfo = roverLine.split(" ");

		return new Rover(Integer.parseInt(roverInfo[0]), 
				Integer.parseInt(roverInfo[1]), 
				roverInfo[2].charAt(0));
	}

	private boolean validatePosition(String roverLine, String[] gridDimension) {
		boolean valid = true;
		
		String[] roverInfo = roverLine.split(" ");
		int positionX = Integer.parseInt(roverInfo[0]);
		int gridMaxDimensionX = Integer.parseInt(gridDimension[0]);
		int positionY = Integer.parseInt(roverInfo[1]);
		int gridMaxDimensionY = Integer.parseInt(gridDimension[1]);
		
		if (positionX < 0 || positionX > gridMaxDimensionX)
				valid = false;
		if (positionY < 0 || positionY > gridMaxDimensionY)
			valid = false;
		
		return valid;
	}

}
