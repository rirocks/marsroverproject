package com.mars;

/**
 * Mars Rover Project
 *
 */
public class MarsRover 
{

	public static void main( String[] args ) {
		CommandHandler commandCenter = new CommandHandler();

		commandCenter.startMission(args[0]);
	}
}
