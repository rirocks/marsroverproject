# MarsRoverProject

This is the solution for The Mars Rover programming problem, as part of the technical assignment for ThoughWorks selection process.

Condition

- Have Maven building tool installed.

Assumptions

- It was assumed that the input file is going to be formatted correctly at all times.
- It is assumed that if the Rover gets to the limit of the plateau it won't move further than there.
- It is assumed that more than one Rover can be in the same position on the plateau.

Design decisions 

- Commands different than 'L', 'R' and 'M', will be ignored.

- For scalability it was decided that a producer consumer model would be implemented.This way if we have a large number of Rovers in the file the reading and writing of output can be done in parallel.

- Maven was used as a build tool to help the building. For the unit testing JUnit was used as a Maven dependency.

- The problem was solved creating three (3) different classes and a special data type:
	- Heading: This special data type that was created, it's an enum, this allowed to define the direction the Rover was facing for movement. 
	- Rover: This class defines the Rover object which has the attributes for defining its position and the heading direction, and methods defining the movement, rotation and command processing the Rover needs.
	- CommandHandler: This class is in charge of the file reading and the definition of the producer of rovers when reading the file and the consumer that writes the output with the final position of the rovers, also other methods that help us maintain the rules.

How to run

- To run this solution you will need to have Maven installed.

- After unzipping the file, and if you're reading this you have, then:
	- Open a Terminal or Command Prompt 
	- Navigate to the folder you just unzipped in the Terminal, and then enter the folder named MarsRoverProject
	- Once in the MarsRoverProject folder execute the following commands in the terminal:
		- mvn clean package -> This is going generate the .jar executable file that contains the program in a new folder 		called target.
		- java -jar target/MarsRoverProject-2.0 <inputTestFilePath> -> This will execute the program and the output file will be generated, it will be found on the same folder with the name output. The <inputTestFilePath> is the path of the input test file, for example, in Windows D:\Documents\Personal\test.txt or in Linux ../Documents/Personal/test.txt

- Other Maven commands that can be interesting:
	- mvn compile -> To compile the project
	- mvn test -> To run the unit tests with JUnit
		
	
